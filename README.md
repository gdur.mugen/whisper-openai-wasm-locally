# whisper-openai-wasm-locally

Simple starter for Speech-To-Text web app using openai's whisper.

This project is heavily based on the whisper.cpp project
https://whisper.ggerganov.com/: "High-performance inference of OpenAI's Whisper automatic speech recognition (ASR) model"

However this project should be much easier to start.
It will show the whisper.cpp "stream" example in which what the users says is recorded via the mic and
transcribed periodically into text.

## Getting started

```
git clone https://gitlab.com/gdur.mugen/whisper-openai-wasm-locally.git
cd whisper-openai-wasm-locally
npm i
npm start
```

## Authors and acknowledgment

99% of the code is from made by https://github.com/ggerganov

Repository: https://github.com/ggerganov/whisper.cpp

## License

MIT License (as before)

## Project status

I would like to refine the project further but at least is is a good starting point hopefully.
